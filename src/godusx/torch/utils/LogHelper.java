package godusx.torch.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

import javax.swing.JOptionPane;

import godusx.torch.Main;

public class LogHelper {
	private FileWriter log;

	public LogHelper() {
		try {
			log = new FileWriter("torch.log", false);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Невозможно создать файл логов", "Ошибка", Main.ERR);
			System.exit(1);
		}
	}
	
	private void log(String message, String level) {
		String time = Calendar.DAY_OF_MONTH + "." + Calendar.MONTH + "." + Calendar.YEAR + " " + Calendar.HOUR + ":"
				+ Calendar.MINUTE + ":" + Calendar.SECOND + ":" + Calendar.MILLISECOND;
		try {
			log.write("[" + time + "][" + level + "]# " + message);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Невозможно получить текущую дату", "Ошибка", Main.ERR);
			Main.exit(Main.ERR);
		}
	}
	
	/**
	 * Log some information
	 * @param message
	 */
	public void info(String message) {
		log(message, "INFO");
	}
	
	/**
	 * Log warning
	 * @param message
	 */
	public void warn(String message) {
		log(message, "WARN");
	}
	
	/**
	 * Log error
	 * @param message
	 */
	public void error(String message) {
		log(message, "ERROR");
	}
}
