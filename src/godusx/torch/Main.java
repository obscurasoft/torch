package godusx.torch;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import godusx.torch.utils.LogHelper;

public class Main extends JFrame {
	private static final long serialVersionUID = 3224905208521269801L;
	/** Error number */
	public static final int ERR = 1;
	/** Log helper, use Main.log.info(message) to log some info. */
	public static LogHelper log = new LogHelper();
	/** For reading data.dat */
	public static Scanner data;
	/** For writing in data.dat */
	public static FileWriter file;
	/** Workers array */
	public static String[][] arr = new String[128][128];
	/** Hours array */
	public static int[] hours = new int[128];

	public static void main(String args[]) {
		log.info("Started");
		String arg = "";
		for (String s : args) {
			arg = arg + s;
		}
		log.info("Arguments: " + arg);
		arg = null;
		ReadFile();
	}

	private static void ReadFile() {
		File file = new File("data.dat");
		// Creating file if not exist or dir
		if (!file.exists()) {
			log.warn("Data file not found");
			try {
				file.createNewFile();
				log.info("Successfully created data.dat");
			} catch (IOException e) {
				log.error("Can not create data file!");
				JOptionPane.showMessageDialog(null, "Невозможно создать файл записей", "Ошибка", ERR);
				System.exit(ERR);
			}
		} else if (file.isDirectory()) {
			log.warn("Data file is directory and will be recreated");
			file.delete();
			try {
				file.createNewFile();
				log.info("Successfully created data.dat");
			} catch (IOException e) {
				log.error("Can not create data file!");
				JOptionPane.showMessageDialog(null, "Невозможно создать файл записей", "Ошибка", ERR);
				System.exit(ERR);
			}
		}
		// Checking file
		if (file.exists()) {
			log.info("Found data.dat file");
		} else {
			log.error("File data.dat not found and has not been created");
			JOptionPane.showMessageDialog(null, "Файл записей не найден и не был создан", "Ошибка", ERR);
			System.exit(ERR);
		}
		// Constructing scanner
		try {
			data = new Scanner(file);
		} catch (FileNotFoundException e) {
			log.error("File data.dat not found and has not been created");
			JOptionPane.showMessageDialog(null, "Файл записей не найден и не был создан", "Ошибка", ERR);
			System.exit(ERR);
		}
		// Writing file to string array
		while (data.hasNext()) {
			for (int i = 0; i < arr.length; i++) {
				for (int j = 0; j < arr[i].length; j++) {
					arr[i][j] = data.next();
				}
				hours[i] = data.nextInt();
			}
		}
		file = null;
	}

	private static void WriteFile() {
		File file = new File("data.dat");
		// Checking data.dat, deleting
		if (file.exists()) {
			log.info("Found data.dat file");
			file.delete();
		}
		// Creating new file
		if (!file.exists()) {
			log.warn("Data file not found");
			try {
				file.createNewFile();
				log.info("Successfully created data.dat");
			} catch (IOException e) {
				log.error("Can not create data file!");
				JOptionPane.showMessageDialog(null, "Невозможно создать файл записей", "Ошибка", 1);
				System.exit(1);
			}
		}
		try {
			Main.file = new FileWriter(file);
		} catch (IOException e) {
			log.error("File data.dat not found and has not been created");
			JOptionPane.showMessageDialog(null, "Файл записей не найден и не был создан", "Ошибка", 1);
			System.exit(1);
		}
		// Writing data
		while (data.hasNext()) {
			for (int i = 0; i < arr.length; i++) {
				for (int j = 0; j < arr[i].length; j++) {
					try {
						Main.file.write(arr[i][j]);
					} catch (IOException e) {
						log.error("Can not write to file data.dat");
						JOptionPane.showMessageDialog(null, "Ошибка записи", "Ошибка", ERR);
						System.exit(ERR);
					}
				}
				hours[i] = data.nextInt();
			}
		}
	}
	
	public static void exit(int code) {
		WriteFile();
		System.exit(code);
	}
}
